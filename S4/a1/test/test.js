const { factorial, div_check } = require ('../src/util.js');

//Gets the expect and assert functions from chai to be used
const {expect, assert} = require ('chai');

//Test Suites are made up of collection of test cases that should be executed together

// "describe()" keyword is used to group tests together
describe('test_fun_factorials', () => {


	it('test_fun_factorial_0!_is_1', () => {
		const product = factorial(0);
		expect (product).to.equal(1); //return expected and actual values
	})

	it('test_fun_factorial_4!_is_24', () => {
		const product = factorial(4);
		expect (product).to.equal(24); //return expected and actual values
	})

	it('test_fun_factorial_10!_is_3628800', () => {
		const product = factorial(10);
		expect (product).to.equal(3628800); //return expected and actual values
	})


	it('test_fun_factorial_5!_is_120', () => {
		const product = factorial(5);
		expect (product).to.equal(120); //return expected and actual values
	})


	it('test_fun_factorial_1!_is_1', () => {
		const product = factorial(1);
		expect (product).to.equal(1); //check the function returns correct result
	})

	//Test for negative numbers
	it("test_fun_factorial_neg-1_undefined", () => {
		// RED - Write a test that fails
		const product = factorial(-1);
		expect (product).to.equal(undefined);
	})

// Mini-activity
	/*
	Create a failing test to test a scenario if a user enters data that is not a number.
	- Expect the product to be undefined.
	- test_fun_factorials_invalid_number_is_undefined
	*/
// Add condition in util.js
	// Refactor the code, add factorial

		//Test for negative numbers
	// 1. RED - Write a test that fails
	// 	it("test_fun_factorials_invalid_number_is_undefined", () => {
	// 	const product = "go";
	// 	expect (product).to.equal(undefined);
	// })
	// 2. GREEN Write code to pass the test

	it("test_fun_factorials_invalid_number_is_undefined", () => {
		// RED - Write a test that fails
		const product = factorial("go");
		expect (product).to.equal(undefined);
	})
	
})


// 1. In your test.js, create test cases in the test fun factorials test suite to check if the factorial function's result for 0!, 4!, and 10! using assert and/or expect.
// 2. In your util.js, create a function called div_check in util.js that checks a number if it is divisible by 5 or 7.
// a. If the number received is divisible by 5, return true.
// b. If the number received is divisible by 7, return true.
// c. Return false if otherwise
// 3. Create 4 test cases in a new test suite in test.js that would check if the functionality of div_check is correct.
// 4. Initialize your local git repository, add the remote link and push to git with the commit message of “Add activity code S2".
// 5. Add the link in Boodle.


describe('test_divisibility_by_5_or_7', () => {

	it('test_100_is_divisible_by_5', () => {
		const result = div_check(100);
		assert.equal(result, true); // check if 100 is divisible by 5
	})

	it('test_49_is_divisible_by_7', () => {
		const result = div_check(49);
		assert.equal(result, true); // check if 49 is divisible by 7
	})
	
	it('test_30_is_divisible_by_5', () => {
		const result = div_check(30);
		assert.equal(result, true); // check if 30 is divisible by 5
	})
	
	it('test_56_is_divisible_by_7', () => {
		const result = div_check(56);
		assert.equal(result, true); // check if 56 is not divisible by 7
	})
		
})

