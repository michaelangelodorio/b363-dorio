const { factorial } = require ('../src/util.js');

//Gets the expect and assert functions from chai to be used
const {expect, assert} = require ('chai');

//Test Suites are made up of collection of test cases that should be executed together

// "describe()" keyword is used to group tests together
describe('test_fun_factorials', () => {

	// "it()" accepts two parameters
	// string explaining what the test should do
	// callback function which contains the actual test

	it('test_fun_factorial_5!_is_120', () => {
		const product = factorial(5);
		expect (product).to.equal(120); //return expected and actual values
	})


	// it('test_fun_factorial_5!_is_120', () => {
	// 	const product = factorial(5);
	// 	assert.equal(product, 120); 
	// })


	it('test_fun_factorial_1!_is_1', () => {
		const product = factorial(1);
		assert.equal(product, 1); //check the function returns correct result
	})


	// it('test_fun_factorial_1!_is_1', () => {
	// 	const product = factorial(1);
	// 	expect (product).to.equal(1);  //check the function returns correct result
	// })

	
})


// 1. In your test.js, create test cases in the test fun factorials test suite to check if the factorial function's result for 0!, 4!, and 10! using assert and/or expect.
// 2. In your util.js, create a function called div_check in util.js that checks a number if it is divisible by 5 or 7.
// a. If the number received is divisible by 5, return true.
// b. If the number received is divisible by 7, return true.
// c. Return false if otherwise
// 3. Create 4 test cases in a new test suite in test.js that would check if the functionality of div_check is correct.
// 4. Initialize your local git repository, add the remote link and push to git with the commit message of “Add activity code S2".
// 5. Add the link in Boodle.

