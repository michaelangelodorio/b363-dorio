const chai = require('chai')

const expect = chai.expect;
// same as const {expect} = require("chai");

const http = require('chai-http');
chai.use(http);


describe("api_test_suite_2", () => {
	
	it("test_api_people_is_running", () =>{
		chai.request('http://localhost:5001')
		// "get()" specifies the type of HTTP request
		// accepts the API endpoint
		.get('/people')
		// "end()" is the method that handles the error response that will be received from the endpoint
		.end((err,res)=>{
			// ".not" negates all assertions that follow in the chain
			expect(res).to.not.equal(undefined);
		})
	})

	it("test_api_people_returns_200", (done) =>{
		chai.request('http://localhost:5001')
		// "get()" specifies the type of HTTP request
		// accepts the API endpoint
		.get('/people')
		// "end()" is the method that handles the error response that will be received from the endpoint
		.end((err,res)=>{
			// ".not" negates all assertions that follow in the chain
			expect(res.status).to.equal(200);
			// "done" function is typically used in asynchronous test cases to signal that the case is complete
			done();
		})
	})

	it("test_api_post_person_returns_400_if_no_person_name", (done) =>{
		chai.request('http://localhost:5001')
		// "post()" specifies the type of HTTP request
		// accepts the API endpoint
		.post('/person')
		.type('json')
		.send({
			alias : "Jason",
			age : 28
		})
		// "end()" is the method that handles the error response that will be received from the endpoint
		.end((err,res)=>{
			// ".not" negates all assertions that follow in the chain
			expect(res.status).to.equal(400);
			// "done" function is typically used in asynchronous test cases to signal that the case is complete
			done();
		})
	})


	it("test_api_post_person_is_running", () =>{
		chai.request('http://localhost:5001')
		// "get()" specifies the type of HTTP request
		// accepts the API endpoint
		.post('/person')
		// "end()" is the method that handles the error response that will be received from the endpoint
		.end((err,res)=>{
			// ".not" negates all assertions that follow in the chain
			expect(res).to.not.equal(undefined);
		})
	})

	it("test_api_post_person_returns_400_if_no_ALIAS", (done) =>{
		chai.request('http://localhost:5001')
		// "post()" specifies the type of HTTP request
		// accepts the API endpoint
		.post('/person')
		.type('json')
		.send({
			alias: "Jay",
			firstname : "Jason",
			age : 28
		})
		// "end()" is the method that handles the error response that will be received from the endpoint
		.end((err,res)=>{
			// ".not" negates all assertions that follow in the chain
			expect(res.status).to.equal(400);
			// "done" function is typically used in asynchronous test cases to signal that the case is complete
			done();
		})
	})

	it("test_api_post_person_returns_400_if_no_AGE", (done) =>{
		chai.request('http://localhost:5001')
		// "post()" specifies the type of HTTP request
		// accepts the API endpoint
		.post('/person')
		.type('json')
		.send({
			alias: "Jay",
			firstname : "Jason"
			
		})
		// "end()" is the method that handles the error response that will be received from the endpoint
		.end((err,res)=>{
			// ".not" negates all assertions that follow in the chain
			expect(res.status).to.equal(400);
			// "done" function is typically used in asynchronous test cases to signal that the case is complete
			done();
		})
	})


})