const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {

	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	})


	app.post('/rates', (req, res) => {

  		const { name, ex, alias } = req.body;

        if (!req.body.hasOwnProperty('name')) {
        return res.status(400).send({
            'error': 'Bad Request - missing required parameter NAME'
        });
	    }

	    if (typeof req.body.name !== 'string') {
	        return res.status(400).send({
	            'error': 'Bad Request - NAME has to be a string'
	        });
	    }

	    if (req.body.name === '') {
	        return res.status(400).send({
	            'error': 'Bad Request - NAME cannot be empty'
	        });
	    }

	    if (!req.body.hasOwnProperty('ex')) {
	        return res.status(400).send({
	            'error': 'Bad Request - missing required parameter EX'
	        });
	    }

	    if (typeof req.body.ex !== 'object') {
	        return res.status(400).send({
	            'error': 'Bad Request - EX has to be an object'
	        });
	    }

	    if (Object.keys(req.body.ex).length === 0) {
	        return res.status(400).send({
	            'error': 'Bad Request - EX cannot be empty'
	        });
	    }

	    if (!req.body.hasOwnProperty('alias')) {
	        return res.status(400).send({
	            'error': 'Bad Request - missing required parameter ALIAS'
	        });
	    }

	 	if (!req.body.hasOwnProperty('alias') || typeof req.body.alias !== 'string' || req.body.alias === '') {
	            return res.status(400).send({
	                'error': 'Bad Request - Invalid or missing ALIAS'
	            });
	        }

	//Check if post /currency returns status 400 if all fields are complete but there is a duplicate alias
		 if (alias !=="" && name !=="" && ex !=="") {
		    for (const key in exchangeRates) {
		        if (exchangeRates[key].alias && exchangeRates[key].alias === alias) {
		            return res.status(400).send({
		                'error': 'Alias already exists'
		            });
		        }
		    }

		    return res.status(200).send({
		        'success': 'Good Request - no duplicates', 'rates': exchangeRates
		    });
		}
	// 


	// //Check if post /currency returns status 200 if all fields are complete and there are no duplicates

	})

}
