const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', () => {
	
	it('test_api_get_rates_is_running', () => {
		chai.request('http://localhost:5001').get('/rates')
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
		})
	})
	
	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();	
		})		
	})
	
	// it('test_api_get_rates_returns_object_of_size_5', (done) => {
	// 	chai.request('http://localhost:5001')
	// 	.get('/rates')
	// 	.end((err, res) => {
	// 		expect(Object.keys(res.body.rates)).does.have.length(5);
	// 		done();	
	// 	})		
	// })


	//NAMES
	it('test_api_post_currency_returns_400_if_no_name', (done) => {
    chai.request('http://localhost:5001')
    .post('/rates')
    .type('json')
    .send({
	        ex: {
	            'usd': 0.020,
	            'won': 23.39,
	            'yen': 2.14,
	            'yuan': 0.14
	        }
	    })
	    .end((err, res) => {
	        expect(res.status).to.equal(400);
	        done();
	    });
	});

	it('test_api_post_currency_returns_400_if_name_is_not_string', (done) => {
    chai.request('http://localhost:5001')
    .post('/rates')
    .type('json')
    .send({
    	 	name: 20,
	        ex: {
	            'usd': 0.020,
	            'won': 23.39,
	            'yen': 2.14,
	            'yuan': 0.14
	        }
	    })
	    .end((err, res) => {
	        expect(res.status).to.equal(400);
	        done();
	    });
	});

	it('test_api_post_currency_returns_400_if_name_is_empty', (done) => {
    chai.request('http://localhost:5001')
    .post('/rates')
    .type('json')
    .send({
    	 	name: "",
	        ex: {
	            'usd': 0.020,
	            'won': 23.39,
	            'yen': 2.14,
	            'yuan': 0.14
	        }
	    })
	    .end((err, res) => {
	        expect(res.status).to.equal(400);
	        done();
	    });
	});


	//EXCHANGE RATES

	it('test_api_post_currency_returns_400_if_no_ex', (done) => {
    chai.request('http://localhost:5001')
    .post('/rates')
    .type('json')
    .send({
    		name: "Philippine Peso",
	        rate: {
	            'usd': 0.020,
	            'won': 23.39,
	            'yen': 2.14,
	            'yuan': 0.14
	        }
	    })
	    .end((err, res) => {
	        expect(res.status).to.equal(400);
	        done();
	    });
	});

	it('test_api_post_currency_returns_400_if_ex_is_not_object', (done) => {
	    chai.request('http://localhost:5001')
	    .post('/rates')
	    .type('json')
	    .send({
	    	 	name: "Philippine Peso",
		        ex: 'usd: 0.020'
		    })
		    .end((err, res) => {
		        expect(res.status).to.equal(400);
		        done();
		    });
		});


	it('test_api_post_currency_returns_400_if_ex_is_empty', (done) => {
	    chai.request('http://localhost:5001')
	    .post('/rates')
	    .type('json')
	    .send({
	    	 	name: "Philippine Peso",
		        ex: {}
		    })
		    .end((err, res) => {
		        expect(res.status).to.equal(400);
		        done();
		    });
		});


	// ALIAS
	it('test_api_post_currency_returns_400_if_no_alias', (done) => {
	    chai.request('http://localhost:5001')
	    .post('/rates')
	    .type('json')
	    .send({
			name: "Philippine Peso",
			ex: {	
				'usd': 0.020,
				'won': 23.39,
				'yen': 2.14,
				'yuan': 0.14
				}
		    })
		    .end((err, res) => {
		        expect(res.status).to.equal(400);
		        done();
		    });
		});

	it('test_api_post_currency_returns_400_if_alias_is_not_string', (done) => {
	    chai.request('http://localhost:5001')
	    .post('/rates')
	    .type('json')
	    .send({
	    		alias: {},
	    	 	name: "Philippine Peso",
		       	ex: {	
					'usd': 0.020,
					'won': 23.39,
					'yen': 2.14,
					'yuan': 0.14
				}
		    })
		    .end((err, res) => {
		        expect(res.status).to.equal(400);
		        done();
		    });
		});

	it('test_api_post_currency_returns_400_if_alias_is_empty', (done) => {
	    chai.request('http://localhost:5001')
	    .post('/rates')
	    .type('json')
	    .send({
	    		alias: "",
	    	 	name: "Philippine Peso",
		         	ex: {	
					'usd': 0.020,
					'won': 23.39,
					'yen': 2.14,
					'yuan': 0.14
				}
		    })
		    .end((err, res) => {
		        expect(res.status).to.equal(400);
		        done();
		    });
		});

		//CHECK ALL FIELDS


		it('test_api_post_currency_returns_400_if_duplicate_alias', (done) => {
	    chai.request('http://localhost:5001')
	    .post('/rates')
	    .type('json')
	    .send({
	    		name: "Piso",
    			alias : "php",
	    	 	name: "Philippine Peso",
		         	ex: {	
					'usd': 0.020,
					'won': 23.39,
					'yen': 2.14,
					'yuan': 0.14
				}
		    })
		    .end((err, res) => {
		        expect(res.status).to.equal(400);
		        done();
		    });
		});

	
		it('test_api_post_currency_returns_200_if_no_dups', (done) => {
		  chai.request('http://localhost:5001')
		    .post('/rates')
		    .type('json')
		    .send({
		      alias: "Peso",
		      name: "Philippine Peso",
		      ex: {
		        'usd': 0.020,
		        'won': 23.39,
		        'yen': 2.14,
		        'yuan': 0.14
		      }
		    })
		    .end((err, res) => {
		      expect(res.status).to.equal(200);
		      done();
		    });
		});


})
